#!/bin/bash

set -xe
: "${POSTGRES_HOST?Need an database host}"
cd /src

python manage.py migrate
#python manage.py set_admin || true
python manage.py runserver 0.0.0.0:8000
exec "$@"