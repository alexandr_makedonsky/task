from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from .models import User


class UserCreateSerializer(serializers.ModelSerializer):
    password = serializers.CharField(required=True, write_only=True)
    password_2 = serializers.CharField(required=True, write_only=True)

    class Meta:
        model = User
        fields = [
            'username',
            'password',
            'password_2',
        ]

    def validate(self, attrs):
        password = attrs.get('password')
        confirm_password = attrs.pop('password_2')
        if password != confirm_password:
            raise ValidationError('Passwords are different')
        return attrs

    def create(self, validated_data):
        user = User.objects.create_user(username=validated_data['username'], password=validated_data['password'])
        return user


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        exclude = [
            'password',
        ]
