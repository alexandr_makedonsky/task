from django.core.management.base import BaseCommand
from users.models import User
from django_dynamic_fixture import G


class Command(BaseCommand):
    help = 'Create Admin'

    def handle(self, *args, **kwargs):
        admin = G(User,
                  username='admin',
                  is_active=True,
                  is_staff=True,
                  is_superuser=True)

        admin.set_password('admin')
        admin.save()
