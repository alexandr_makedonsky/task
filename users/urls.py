from django.urls import path, include
from rest_framework.routers import DefaultRouter

from users import viewsets

router = DefaultRouter()
router.register('profile', viewsets.UserViewSet, basename='UserViewSet')

urlpatterns = [
    path('', include(router.urls)),
]
