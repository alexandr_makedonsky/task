from django.contrib import admin
from django.urls import path, include

from rest_framework import permissions
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from users.views import UserCreateView

schema_view = get_schema_view(
    openapi.Info(
        title="TEST TASK API",
        default_version='v1',
        description="NO DESCRIPTION",
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

api = [
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('survey/', include('apps.survey.urls')),
    path('hometask/', include('apps.hometask.urls')),
    path('users/', include('users.urls')),
    path('auth/register/', UserCreateView.as_view(), name='user-register'),

]

urlpatterns = [

    path('docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('admin/', admin.site.urls),
    path('v1/', (api, 'api', 'api')),

]
