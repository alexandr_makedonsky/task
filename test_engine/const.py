# FILE_SIZE
FILE_SIZE = 2097152

COMPLEXITY = [
    ('Casual', 'Casual'),
    ('Medium', 'Medium'),
    ('Veteran', 'Veteran'),
    ('Dark Souls', 'Dark Souls'),
]