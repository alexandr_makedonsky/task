from django.urls import path, include
from rest_framework.routers import DefaultRouter

from apps.survey.viewsets import SurveyViewSet, SurveyResultViewSet, QuestionViewSet, TopicViewSet


router = DefaultRouter()
router.register('topic', TopicViewSet, basename='TopicViewSet')
router.register('survey', SurveyViewSet, basename='SurveyViewSet')
router.register('question', QuestionViewSet, basename='QuestionViewSet')
router.register('result', SurveyResultViewSet, basename='SurveyResultViewSet')


urlpatterns = [
    path('test/', include(router.urls)),
]
