from apps.survey.models import Topic
from apps.survey.serializers import TopicSerializer
from rest_framework import viewsets, permissions


class TopicViewSet(viewsets.ModelViewSet):
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer
    permission_classes = [permissions.IsAdminUser]
