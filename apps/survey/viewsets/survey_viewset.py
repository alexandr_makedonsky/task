from rest_framework import mixins, viewsets, permissions

from apps.survey.models import Survey, Result
from apps.survey.serializers import SurveySerializer, SurveyPublicSerializer, \
    SurveyCreateSerializer, ResultSerializer


class SurveyViewSet(viewsets.ModelViewSet):
    """ Survey crud"""
    queryset = Survey.objects.all()
    serializers = {
        "create": SurveyCreateSerializer,
        "list": SurveyPublicSerializer,
        "retrieve": SurveySerializer,
        "update": SurveySerializer,
        "delete": SurveySerializer
    }
    permission_classes = [permissions.IsAdminUser]

    def get_serializer_class(self):
        return self.serializers[self.action]


class SurveyPublicViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """ View to see list of surveys"""
    queryset = Survey.objects.all()
    serializer_class = SurveySerializer


class SurveyResultViewSet(
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet):
    """
    CREATE: send answers and get result
    LIST: show user's result
    RETRIEVE: show result by ID
    """

    queryset = Result.objects.all()
    serializer_class = ResultSerializer

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user)
