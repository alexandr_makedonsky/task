from django.db import transaction

from apps.survey.models import Result, ResultData


class ResultCreateUseCase:

    def __call__(self,
                 data
                 ) -> Result:
        with transaction.atomic():
            result = Result.objects.create(user=data['user'], survey=data['survey'])
            answers = []
            for answer in data['answers']:
                correct_answers = list(answer['answers_ids'])
                user_answers = list(answer['question'].get_correct_answer())

                is_correct = correct_answers == user_answers

                result_data = ResultData.objects.create(question=answer['question'], correct=is_correct)
                result_data.answers_ids.add(*answer['answers_ids'])

                answers.append(result_data)

            result.answers.add(*answers)

            return result
