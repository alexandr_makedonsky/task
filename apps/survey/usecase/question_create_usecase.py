from rest_framework.exceptions import ValidationError  # noqa
from django.db import transaction

from apps.survey.models import Question, Answer


class QuestionCreateUseCase:

    def __call__(self,
                 question_data: dict,
                 answer_data: list
                 ) -> Question:
        survey = question_data['survey']
        with transaction.atomic():
            question = Question.objects.create(
                survey=survey,
                text=question_data['text'],
                mark=question_data['mark'],
                count_correct_answers=question_data['count_correct_answers'],
            )

            answers = [Answer(question=question, **answer) for answer in answer_data]
            Answer.objects.bulk_create(answers)

        return question

    # def is_survey_exists(self, survey_pk: int):
    #     if not Survey.objects.filter(pk=survey_pk).exists():
    #         raise ValidationError('Survey doesn\'t exists')
