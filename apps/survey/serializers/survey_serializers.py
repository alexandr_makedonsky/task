from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from apps.survey.models import Survey, Question, Answer
from apps.survey.usecase import QuestionCreateUseCase


class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = [
            'id',
            'text',
            'is_correct'
        ]


class SurveyAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = [
            'id',
            'text',
        ]


class QuestionSerializer(serializers.ModelSerializer):
    answers = AnswerSerializer(many=True)

    class Meta:
        model = Question
        fields = [
            'id',
            'survey',
            'text',
            'mark',
            'count_correct_answers',
            'answers'
        ]

    def validate(self, attrs):

        if not attrs['answers'] or attrs['count_correct_answers']:
            raise ValidationError(
                'Can\'t create  question without required fields'
            )

        correct_answers = list(filter(lambda x: x['is_correct'], attrs['answers']))

        if attrs['count_correct_answers'] != len(correct_answers):
            raise ValidationError(
                'Count of correct answers don\'t match with question\'s correct answers count'
            )

        return attrs

    def create(self, validated_data):

        answers = validated_data.pop('answers')
        create_question = QuestionCreateUseCase()
        return create_question(
            question_data=validated_data, answer_data=answers
        )


class SurveyQuestionSerializer(serializers.ModelSerializer):
    answers = SurveyAnswerSerializer(many=True)

    class Meta:
        model = Question
        fields = [
            'id',
            'text',
            'mark',
            'answers'

        ]


class SurveyPublicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Survey
        fields = [
            'id',
            'title',
            'topic',
        ]


class SurveySerializer(serializers.ModelSerializer):
    class Meta:
        model = Survey
        fields = [
            'id',
            'title',
            'topic',
        ]


class SurveyCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Survey
        fields = [
            'title',
            'topic',
        ]
