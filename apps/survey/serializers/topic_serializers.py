from rest_framework import serializers

from apps.survey.models import Topic


class TopicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Topic
        fields = [
            'id',
            'name',
            'slug',
        ]


class TopicCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Topic
        fields = [
            'name',
            'slug',
        ]
