from rest_framework import serializers

from apps.survey.models import Result, ResultData, Answer
from apps.survey.usecase import ResultCreateUseCase


class ResultDataSerializer(serializers.ModelSerializer):
    answers_ids = serializers.PrimaryKeyRelatedField(many=True, queryset=Answer.objects)

    class Meta:
        model = ResultData
        fields = [
            'question',
            'answers_ids'
        ]


class ResultSerializer(serializers.ModelSerializer):
    answers = ResultDataSerializer(many=True)

    class Meta:
        model = Result
        fields = [
            'user',
            'survey',
            'answers',
            'result'
        ]

    def create(self, validated_data):
        result = ResultCreateUseCase()
        return result(validated_data)
