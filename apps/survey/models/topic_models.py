from django.db import models
from uuslug import uuslug


class Topic(models.Model):

    name = models.CharField(
        max_length=40
    )

    slug = models.SlugField(
        verbose_name='slug', max_length=255, unique=True
    )

    def save(self, *args, **kwargs):
        self.slug = uuslug(self.name, instance=self)

        super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name = 'Topic'
        verbose_name_plural = 'Topics'
