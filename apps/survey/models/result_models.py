from django.db import models


class Result(models.Model):
    user = models.ForeignKey('users.User', on_delete=models.CASCADE)
    survey = models.ForeignKey('Survey', on_delete=models.CASCADE, related_name='controls')
    answers = models.ManyToManyField('ResultData')

    @property
    def result(self):
        user_correct_answers = self.answers.all().filter(correct=True)
        mark = sum([q.question.mark for q in user_correct_answers])
        return mark


def __str__(self):
    return f"{self.survey}"


class ResultData(models.Model):
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
    answers_ids = models.ManyToManyField('Answer', related_name='answers_ids')
    correct = models.BooleanField()

    def __str__(self):
        return f"{self.pk}"
