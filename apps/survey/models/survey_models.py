from django.db import models


class Survey(models.Model):

    title = models.CharField(
        max_length=48, verbose_name='Title'
    )
    topic = models.ForeignKey(
        'Topic', on_delete=models.CASCADE
    )
    active = models.BooleanField(
        default=True
    )

    def __str__(self):
        return f'{self.topic} - {self.title}'


class Question(models.Model):

    survey = models.ForeignKey(
        'Survey', on_delete=models.CASCADE, related_name='questions', verbose_name='Test'
    )
    text = models.CharField(
        max_length=160, verbose_name='Question'
    )
    mark = models.PositiveIntegerField(
        default=0, verbose_name='Mark'
    )
    count_correct_answers = models.PositiveIntegerField(
        default=1, verbose_name='Count of correct answers', blank=False, null=False
    )

    def get_correct_answer(self):
        return self.answers.filter(is_correct=True)

    def __str__(self):
        return f'{self.text}'


class Answer(models.Model):

    question = models.ForeignKey(
        'Question', on_delete=models.CASCADE, verbose_name='Question', related_name='answers'
    )
    text = models.CharField(
        max_length=160, verbose_name='Answer'
    )
    is_correct = models.BooleanField(
        default=False
    )

    def __str__(self):
        return f'{self.text}'
