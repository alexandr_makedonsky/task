import django_filters
from django_filters import rest_framework as filters

from apps.survey.models import Survey, Question


class SurveyFilter(django_filters.FilterSet):
    title = filters.CharFilter(field_name='title', lookup_expr='icontains')
    topic = filters.CharFilter(field_name='topic__name', lookup_expr='icontains')
    topic_id = filters.NumberFilter(field_name='topic__id', lookup_expr='exact')
    active = filters.CharFilter(field_name='active', lookup_expr='icontains')

    class Meta:
        model = Survey
        fields = [
            'title',
            'topic',
            'active',
        ]


class QuestionFilter(django_filters.FilterSet):
    survey_id = filters.NumberFilter(field_name='survey_id', lookup_expr='exact')
    survey = filters.CharFilter(field_name='survey_title', lookup_expr='icontains')

    class Meta:
        model = Question
        fields = [
            'survey'
        ]
