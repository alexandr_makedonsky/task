from django.contrib import admin
from apps.survey.models import Survey, Answer, Question, Topic, Result, ResultData

admin.site.register(Survey)
admin.site.register(Answer)
admin.site.register(Question)
admin.site.register(Topic)
admin.site.register(Result)
admin.site.register(ResultData)
