from rest_framework import viewsets, mixins, permissions

from apps.hometask.models import HomeworkSolution
from apps.hometask.serializers import HomeworkSolutionSerializer, HomeworkSolutionCreateSerializer


class HomeworkSolutionListView(mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = HomeworkSolution.objects.all()
    serializer_class = HomeworkSolutionSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        if self.request.user.is_staff:
            return self.queryset
        return self.queryset.filter(user=self.request.user)


class UploadSolutionView(mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = HomeworkSolution.objects.all()
    serializer_class = HomeworkSolutionCreateSerializer
    permission_classes = [permissions.IsAuthenticated]
