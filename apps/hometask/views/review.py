from rest_framework import mixins, viewsets, permissions
from apps.hometask import models, serializers


class ReviewViewSet(
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet
):
    permission_classes = (permissions.IsAdminUser,)
    queryset = models.Review.objects.all()
    serializers = {
        'list': serializers.ReviewSerializer,
        'retrieve': serializers.ReviewSerializer,
        'create': serializers.ReviewCreateSerializer,
    }

    def get_queryset(self, **kwargs):
        user = self.request.user
        if user.is_staff:
            return self.queryset
        return self.queryset.filter(user=self.request.user)

    def get_serializer_class(self):
        return self.serializers[self.action]
