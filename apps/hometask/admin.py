from django.contrib import admin
from apps.hometask.models import Homework, HomeworkSolution, Review

admin.site.register(Homework)
admin.site.register(HomeworkSolution)
admin.site.register(Review)
