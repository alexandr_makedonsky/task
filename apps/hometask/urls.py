from django.urls import path, include
from rest_framework.routers import DefaultRouter

from apps.hometask.views import UploadSolutionView, HomeworkSolutionListView, ReviewViewSet

router = DefaultRouter()
router.register('upload-solution', UploadSolutionView, basename='UploadSolutionView')
router.register('solutions', HomeworkSolutionListView, basename='HomeworkSolutionListView')
router.register('review', ReviewViewSet, basename='ReviewViewSet')

urlpatterns = [
    path('', include(router.urls)),
]
