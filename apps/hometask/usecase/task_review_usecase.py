from apps.hometask.models import Review, HomeworkSolution


class ReviewCreateUseCase:
    def __call__(self,
                 value: int,
                 solution: HomeworkSolution,
                 body: str = None
                 ) -> Review:
        review = Review.objects.create(
            value=value,
            body=body,
            solution=solution
        )

        return review
