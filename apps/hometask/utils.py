from test_engine import const


def file_size_check(file):
    return file.size > const.FILE_SIZE
