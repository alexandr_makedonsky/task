from rest_framework import serializers

from apps.hometask.models import Review
from apps.hometask import usecase


class ReviewSerializer(serializers.ModelSerializer):

    class Meta:
        model = Review
        fields = "__all__"


class ReviewCreateSerializer(serializers.ModelSerializer):
    value = serializers.ChoiceField(choices=Review.RATING_CHOICES, required=True)
    body = serializers.CharField(required=False, allow_blank=True)

    def create(self, validated_data):
        value = validated_data['value']
        body = validated_data['body']
        solution = validated_data['solution']

        review_create = usecase.ReviewCreateUseCase()
        instance = review_create(
            value=value,
            body=body,
            solution=solution
        )

        return instance

    def update(self, instance, validated_data):
        raise NotImplementedError

    class Meta:
        model = Review
        fields = [
            'value',
            'body',
            'solution'
        ]
