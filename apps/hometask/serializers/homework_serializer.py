from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from django.utils import timezone

from apps.hometask.models import Homework, HomeworkSolution
from apps.hometask.utils import file_size_check


class HomeworkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Homework
        fields = '__all__'


class HomeworkSolutionCreateSerializer(serializers.ModelSerializer):
    file = serializers.FileField(write_only=True)

    class Meta:
        model = HomeworkSolution
        fields = [
            'homework',
            'file'
        ]

    def create(self, validated_data):
        date_now = timezone.now()
        if date_now > validated_data['homework'].expiration_date:
            raise ValidationError(
                'Too late. Time to pass homework is expired'
            )

        if not file_size_check(validated_data['file']):
            raise ValidationError("File is too big")

        validated_data['user'] = self.context['request'].user
        return super().create(validated_data)


class HomeworkSolutionSerializer(serializers.ModelSerializer):
    class Meta:
        model = HomeworkSolution
        fields = "__all__"
