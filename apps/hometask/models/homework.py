from django.db import models

from test_engine.const import COMPLEXITY


class Homework(models.Model):


    topic = models.ForeignKey(
        'survey.Topic', verbose_name='Topic', on_delete=models.SET_NULL, blank=True, null=True
    )

    description = models.TextField(
        verbose_name='Description'
    )

    complexity = models.CharField(max_length=10, choices=COMPLEXITY)

    expiration_date = models.DateTimeField(verbose_name='Expiration date')

    def __str__(self):
        return f'{self.pk} -- {self.topic}'


class HomeworkSolution(models.Model):
    homework = models.ForeignKey('Homework', on_delete=models.CASCADE)
    user = models.ForeignKey('users.User', on_delete=models.CASCADE)
    file = models.FileField(upload_to='hometask/', blank=False, null=False)

    def __str__(self):
        return f'{self.homework} -- {self.user}'
