from django.db import models
from django.utils.translation import ugettext_lazy as _


class Review(models.Model):
    RATING_CHOICES = (
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5'),
    )
    value = models.IntegerField(
        choices=RATING_CHOICES, verbose_name=_('value'), default=5
    )
    body = models.TextField(
        verbose_name=_('description'), max_length=100
    )

    solution = models.ForeignKey(
        'HomeworkSolution', related_name='reviews', verbose_name="solution", on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = 'Review'
        verbose_name_plural = 'Reviews'

    def __str__(self):
        return f'Review:{self.value}'
