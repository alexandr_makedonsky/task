FROM python:3.8

RUN mkdir -p /src
WORKDIR /src

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
RUN apt-get update -q && apt-get install -qqy netcat python3-dev python3-pip --no-install-recommends

RUN pip install --upgrade pip
ADD . /src/
ADD requirements.txt .
RUN pip install -r requirements.txt


ENTRYPOINT ["/src/entrypoint.sh"]
